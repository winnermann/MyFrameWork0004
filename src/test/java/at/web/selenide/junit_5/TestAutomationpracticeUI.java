package at.web.selenide.junit_5;

import org.junit.jupiter.api.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class TestAutomationpracticeUI {

    @BeforeAll
    public static void startBrowser(){
        System.out.println("startBrowser");
        AutomationpracticeUI.startBrowser();
    }

    @Order(1)
    @Test
    @DisplayName("Cценарий: login")
    public void login() {
        AutomationpracticeUI.login();

    }

    @AfterAll
    public static void closeBrowser(){
        AutomationpracticeUI.closeBrowser();
        System.out.println("closeBrowser");

    }
}
