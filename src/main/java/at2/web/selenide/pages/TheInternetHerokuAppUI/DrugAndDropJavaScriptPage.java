package at2.web.selenide.pages.TheInternetHerokuAppUI;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

/**
 * PageObject
 * Страница DrugAndDropJavaScriptPage с элементами
 */
public class DrugAndDropJavaScriptPage {
    //Колонка А
    private SelenideElement columnAOnDrugAndDropJavaScriptPage = $("#column-a");
    //Колонка B
    private SelenideElement columnBOnDrugAndDropJavaScriptPage = $("#column-b");

//    //Проверяет текст в колонке "A"
//    public void shouldCheckColumnATextOnDrugAndDropJavaScriptPage(String text){
//        this.columnAOnDrugAndDropJavaScriptPage.shouldHave(Condition.text(text));
//    }
//
//    //Проверяет текст в колонке "B"(в данном случае не испоьзуется)
//    public void shouldCheckColumnBTextOnDrugAndDropJavaScriptPage(String text){
//        this.columnBOnDrugAndDropJavaScriptPage.shouldHave(Condition.text(text));
//    }


    public SelenideElement getColumnAOnDrugAndDropJavaScriptPage() {
        return columnAOnDrugAndDropJavaScriptPage;
    }

    public void setColumnAOnDrugAndDropJavaScriptPage(SelenideElement columnAOnDrugAndDropJavaScriptPage) {
        this.columnAOnDrugAndDropJavaScriptPage = columnAOnDrugAndDropJavaScriptPage;
    }

    public SelenideElement getColumnBOnDrugAndDropJavaScriptPage() {
        return columnBOnDrugAndDropJavaScriptPage;
    }

    public void setColumnBOnDrugAndDropJavaScriptPage(SelenideElement columnBOnDrugAndDropJavaScriptPage) {
        this.columnBOnDrugAndDropJavaScriptPage = columnBOnDrugAndDropJavaScriptPage;
    }
}
