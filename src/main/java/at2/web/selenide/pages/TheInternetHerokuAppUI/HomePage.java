package at2.web.selenide.pages.TheInternetHerokuAppUI;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.element;

/**
 * PageObject
 * Страница HomePage с элементами
 * textWelcomeOnHomePage - Слова "Welcome to the-internet" на странице "HomePage"
 */
public class HomePage {
    //Слова "Welcome to the-internet" на странице "HomePage"
    private SelenideElement textWelcomeOnHomePage = element(By.xpath("//h1[@class=\"heading\"]"));
    //Слова "Checkboxes" на странице "HomePage"
    private SelenideElement textCheckboxesOnHomePage = element(By.cssSelector("#content li:nth-child(6) a"));
    //Слова "Dropdown" на странице "HomePage"
    private SelenideElement textDropdownOnHomePage = element(By.cssSelector("#content li:nth-child(11) a"));
    //Слова "DragAndDrop" на странице "HomePage"
    private SelenideElement textDragAndDropOnHomePage = element(By.cssSelector("#content li:nth-child(10) a"));

//    //Убедиться что на странице есть слова "Welcome to the-internet"
//    public SelenideElement shouldCheckWelcomeTextOnHomePage(String text){
//        return this.textWelcomeOnHomePage.shouldHave(Condition.text(text));
//    }
//
//    //Убедиться что ссылка содержит слова "Checkboxes" и перейти по ссылке
//    public void shouldCheckCheckboxesTextOnHomePage(String text){
//        this.textCheckboxesOnHomePage.shouldHave(Condition.text(text)).click();
//    }
//
//    //Убедиться что ссылка содержит слова "Dropdown" и перейти по ссылке
//    public void shouldCheckDropdownTextOnHomePage(String text){
//        this.textDropdownOnHomePage.shouldHave(Condition.text(text)).click();
//    }
//
//    //Убедиться что ссылка содержит слова "DragAndDrop" и перейти по ссылке
//    public void shouldCheckDragAndDropTextOnHomePage(String text){
//        this.textDragAndDropOnHomePage.shouldHave(Condition.text(text)).click();
//    }

    public SelenideElement getTextWelcomeOnHomePage() {
        return textWelcomeOnHomePage;
    }

    public void setTextWelcomeOnHomePage(SelenideElement textWelcomeOnHomePage) {
        this.textWelcomeOnHomePage = textWelcomeOnHomePage;
    }

    public SelenideElement getTextCheckboxesOnHomePage() {
        return textCheckboxesOnHomePage;
    }

    public void setTextCheckboxesOnHomePage(SelenideElement textCheckboxesOnHomePage) {
        this.textCheckboxesOnHomePage = textCheckboxesOnHomePage;
    }

    public SelenideElement getTextDropdownOnHomePage() {
        return textDropdownOnHomePage;
    }

    public void setTextDropdownOnHomePage(SelenideElement textDropdownOnHomePage) {
        this.textDropdownOnHomePage = textDropdownOnHomePage;
    }

    public SelenideElement getTextDragAndDropOnHomePage() {
        return textDragAndDropOnHomePage;
    }

    public void setTextDragAndDropOnHomePage(SelenideElement textDragAndDropOnHomePage) {
        this.textDragAndDropOnHomePage = textDragAndDropOnHomePage;
    }
}
