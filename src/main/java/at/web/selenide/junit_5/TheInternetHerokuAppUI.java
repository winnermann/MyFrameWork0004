package at.web.selenide.junit_5;

import at.web.selenide.pages.TheInternetHerokuAppUI.CheckBoxPage;
import at.web.selenide.pages.TheInternetHerokuAppUI.DropdownMenuPage;
import at.web.selenide.pages.TheInternetHerokuAppUI.DrugAndDropJavaScriptPage;
import at.web.selenide.pages.TheInternetHerokuAppUI.HomePage;
import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.*;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class TheInternetHerokuAppUI {
    static HomePage homePage = new HomePage();
    static CheckBoxPage checkBoxPage = new CheckBoxPage();
    static DropdownMenuPage dropdownMenuPage = new DropdownMenuPage();
    static DrugAndDropJavaScriptPage drugAndDropJavaScriptPage = new DrugAndDropJavaScriptPage();
    public static void startBrowser(){
        //Открыть браузер
        System.setProperty("selenide.browser", "chrome");
        Configuration.browser = "chrome";
        Configuration.startMaximized = true;
        Configuration.timeout = 6000;
        open("http://the-internet.herokuapp.com");
        //Выводит  заголовок страницы в консоль
        System.out.println(Selenide.title());
        //Проверяет, что заголовок страницы правильный
        assertTrue(Selenide.title().equals("The Internet"));

    }

    public static void checkBox(){
        //Убедиться что на странице есть слова "Welcome to the-internet"
        //element(By.cssSelector("#content h1")).shouldHave(text("Welcome to the-internet"));
        homePage.shouldCheckWelcomeTextOnHomePage("Welcome to the-internet");
        //Убедиться что ссылка содержит слова "Checkboxes" и перейти по ссылке
        //element(By.cssSelector("#content li:nth-child(6) a")).shouldHave(text("Checkboxes")).click();
        homePage.shouldCheckCheckboxesTextOnHomePage("Checkboxes");

        //Проверяет что Чек-бокс не выбран
        //$(By.cssSelector("#checkboxes input[type=checkbox]:nth-child(1)")).shouldNotBe(checked);
        checkBoxPage.makeSureCheckBox1IsNotSelected();
        //Проставляет Чек-бокс
        //$(By.cssSelector("#checkboxes input[type=checkbox]:nth-child(1)")).setSelected(true);
        checkBoxPage.setCheckbox1Selected();
        //Проверяет что Чек-бокс выбран
        //$(By.cssSelector("#checkboxes input[type=checkbox]:nth-child(1)")).shouldBe(checked);
        checkBoxPage.makeSureCheckBox1IsSelected();

        //Проверяет что Чек-бокс выбран
        //$(By.cssSelector("#checkboxes input[type=checkbox]:nth-child(3)")).shouldBe(checked);
        checkBoxPage.makeSureCheckBox2IsSelected();
        //Снимает Чек-бокс
        //$(By.cssSelector("#checkboxes input[type=checkbox]:nth-child(3)")).setSelected(false);
        checkBoxPage.setCheckbox2Unselected();
        //Проверяет что Чек-бокс не выбран
        //$(By.cssSelector("#checkboxes input[type=checkbox]:nth-child(3)")).shouldNotBe(checked);
        checkBoxPage.makeSureCheckBox2IsNotSelected();

    }


    public static void dropDownMenu(){
        //Открыть браузер
        startBrowser();

        //Убедиться что на странице есть слова "Welcome to the-internet"
        homePage.shouldCheckWelcomeTextOnHomePage("Welcome to the-internet");
        //Убедиться что ссылка содержит слова "Dropdown" и перейти по ссылке
        homePage.shouldCheckDropdownTextOnHomePage("Dropdown");


        //Выбрать из выпадающего меню Option 2
        dropdownMenuPage.shouldSelectOption2();
        //проверяет что меню Option 2 выбрано
        dropdownMenuPage.shouldCheckOption2IsSelected();


        //Выбрать из выпадающего меню Option 1
        //parentDiv.find("option:nth-child(2)").scrollTo().click();
        dropdownMenuPage.shouldSelectOption1();
        //проверяет что меню Option 1 выбрано
        //parentDiv.find("option:nth-child(2)").shouldHave(text("Option 1"));
        dropdownMenuPage.shouldCheckOption1IsSelected();
    }

    public static void drugAndDropJavaScript() throws IOException {
        //Открыть браузер
        startBrowser();

        //Убедиться что на странице есть слова "Welcome to the-internet"
        homePage.shouldCheckWelcomeTextOnHomePage("Welcome to the-internet");
        //Убедиться что ссылка содержит слова "Drag and Drop" и перейти по ссылке
        homePage.shouldCheckDragAndDropTextOnHomePage("Drag and Drop");

        //Проверяет что элемент КолонкаА содержит текст А
        //element(By.cssSelector("#column-a")).shouldHave(text("A"));
        drugAndDropJavaScriptPage.shouldCheckColumnATextOnDrugAndDropJavaScriptPage("A");

        //Создадим объект класса File
        File dnd_javascript = new File("scripts/dnd.js");
        FileReader reader = new FileReader(dnd_javascript);
        BufferedReader bufferedReader = new BufferedReader(reader);
        StringBuilder builder = new StringBuilder();
        bufferedReader.lines().forEach(
                o->builder.append(o).append('\n')
        );

        String javaScript = builder.toString();
        javaScript = javaScript + " simulateDragDrop(document.getElementById('column-a'),document.getElementById('column-b'));";
        Selenide.executeJavaScript(javaScript);

        //Проверяет что элемент КолонкаА содержит текст Б
        //element(By.cssSelector("#column-a")).shouldHave(text("B"));
        drugAndDropJavaScriptPage.shouldCheckColumnATextOnDrugAndDropJavaScriptPage("B");

    }

    public static void closeBrowser(){
        Selenide.clearBrowserCookies();
        Selenide.closeWindow();
    }
}
