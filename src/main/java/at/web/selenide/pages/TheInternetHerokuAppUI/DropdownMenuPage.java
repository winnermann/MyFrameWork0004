package at.web.selenide.pages.TheInternetHerokuAppUI;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.*;

/**
 * PageObject
 * Страница DropdownMenuPage с элементами
 */
public class DropdownMenuPage {

    //Выпадающее меню (родительское)
    private SelenideElement parentDiv = $("#dropdown");
    //Выпадающее меню (option1)
    private SelenideElement option1 = $("option:nth-child(2)");
    //Выпадающее меню (option2)
    private SelenideElement option2 = $("option:nth-child(3)");


    public SelenideElement getParentDiv() {
        return parentDiv;
    }

    public void setParentDiv(SelenideElement parentDiv) {
        this.parentDiv = parentDiv;
    }

    public SelenideElement getOption1() {
        return option1;
    }

    public void setOption1(SelenideElement option1) {
        this.option1 = option1;
    }

    public SelenideElement getOption2() {
        return option2;
    }

    public void setOption2(SelenideElement option2) {
        this.option2 = option2;
    }

    public void shouldSelectOption1(){
        this.parentDiv.find("option:nth-child(2)").scrollTo().click();
    }

    public void shouldCheckOption1IsSelected(){
        parentDiv.find("option:nth-child(2)").shouldHave(text("Option 1"));
    }


    public void shouldSelectOption2(){
        this.parentDiv.find("option:nth-child(3)").scrollTo().click();
    }

    public void shouldCheckOption2IsSelected(){
        this.parentDiv.find("option:nth-child(3)").shouldHave(text("Option 2"));
    }

}
