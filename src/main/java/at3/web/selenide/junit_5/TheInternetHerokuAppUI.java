package at3.web.selenide.junit_5;

import at3.web.selenide.pages.TheInternetHerokuAppUI.*;
import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;

import java.io.*;

import static com.codeborne.selenide.Condition.checked;
import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class TheInternetHerokuAppUI {
    static HomePage homePage = new HomePage();
    static CheckBoxPage checkBoxPage = new CheckBoxPage();
    static DropdownMenuPage dropdownMenuPage = new DropdownMenuPage();
    static DrugAndDropJavaScriptPage drugAndDropJavaScriptPage = new DrugAndDropJavaScriptPage();
    static DownloadFilePage downloadFilePage = new DownloadFilePage();
    public static void startBrowser(){
        //Открыть браузер
        System.setProperty("selenide.browser", "chrome");
        Configuration.browser = "chrome";
        Configuration.startMaximized = true;
        Configuration.timeout = 6000;
        open("http://the-internet.herokuapp.com");
        //Выводит  заголовок страницы в консоль
        System.out.println(Selenide.title());
        //Проверяет, что заголовок страницы правильный
        assertTrue(Selenide.title().equals("The Internet"));

    }

    public static void checkBox(){
        //Убедиться что на странице есть слова "Welcome to the-internet"
        assertEquals(homePage.getTextWelcomeOnHomePage().getOwnText(), "Welcome to the-internet");
        //Убедиться что ссылка содержит слова "Checkboxes"
        assertEquals(homePage.getTextCheckboxesOnHomePage().getOwnText(), "Checkboxes");
        //перейти по ссылке "Checkboxes"
        homePage.getTextCheckboxesOnHomePage().click();

        //Проверяет что Чек-бокс не выбран
        checkBoxPage.getCheckbox1OnCheckBoxPage().shouldNotBe(checked);
        //Проставляет Чек-бокс
        checkBoxPage.getCheckbox1OnCheckBoxPage().setSelected(true);
        //Проверяет что Чек-бокс выбран
        checkBoxPage.getCheckbox1OnCheckBoxPage().shouldBe(checked);

        //Проверяет что Чек-бокс выбран
        checkBoxPage.getCheckbox2OnCheckBoxPage().shouldBe(checked);
        //Снимает Чек-бокс
        checkBoxPage.getCheckbox2OnCheckBoxPage().setSelected(false);
        //Проверяет что Чек-бокс не выбран
        checkBoxPage.getCheckbox2OnCheckBoxPage().shouldNotBe(checked);

    }


    public static void dropDownMenu(){
        //Открыть браузер
        startBrowser();

        //Убедиться что на странице есть слова "Welcome to the-internet"
        assertEquals(homePage.getTextWelcomeOnHomePage().getOwnText(), "Welcome to the-internet");
        //Убедиться что ссылка содержит слова "Checkboxes"
        assertEquals(homePage.getTextDropdownOnHomePage().getOwnText(), "Dropdown");
        //перейти по ссылке "Checkboxes"
        homePage.getTextDropdownOnHomePage().click();


        //Выбрать из выпадающего меню Option 2
        //dropdownMenuPage.getParentDiv("#dropdown").find("option:nth-child(3)").scrollTo().click();
        dropdownMenuPage.getParentDiv("#dropdown").selectOption(2);

        //проверяет что меню Option 2 выбрано
        //dropdownMenuPage.getParentDiv("#dropdown").find("option:nth-child(3)").shouldHave(text("Option 2"));
        assertEquals(dropdownMenuPage.getParentDiv("#dropdown").getSelectedOption().getOwnText(), "Option 2");


        //Выбрать из выпадающего меню Option 1
        //dropdownMenuPage.getParentDiv("#dropdown").find("option:nth-child(2)").scrollTo().click();
        dropdownMenuPage.getParentDiv("#dropdown").selectOptionByValue(String.valueOf(1));

        //проверяет что меню Option 1 выбрано
        //dropdownMenuPage.getParentDiv("#dropdown").find("option:nth-child(2)").shouldHave(text("Option 1"));
        assertEquals(dropdownMenuPage.getParentDiv("#dropdown").getSelectedOption().getOwnText(), "Option 1");
    }

    public static void drugAndDropJavaScript() throws IOException {
        //Открыть браузер
        startBrowser();

        //Убедиться что на странице есть слова "Welcome to the-internet"
        assertEquals(homePage.getTextWelcomeOnHomePage().getOwnText(), "Welcome to the-internet");
        //Убедиться что ссылка содержит слова "Drag and Drop"
        assertEquals(homePage.getTextDragAndDropOnHomePage().getOwnText(), "Drag and Drop");
        //перейти по ссылке "Drag and Drop"
        homePage.getTextDragAndDropOnHomePage().click();


        //Проверяет что элемент КолонкаА содержит текст А
        //element(By.cssSelector("#column-a")).shouldHave(text("A"));
        //drugAndDropJavaScriptPage.shouldCheckColumnATextOnDrugAndDropJavaScriptPage("A");
        drugAndDropJavaScriptPage.getColumnAOnDrugAndDropJavaScriptPage().shouldHave(text("A"));
        //assertEquals(drugAndDropJavaScriptPage.getColumnAOnDrugAndDropJavaScriptPage().getOwnText(), "A");

        //Создадим объект класса File
        File dnd_javascript = new File("scripts/dnd.js");
        FileReader reader = new FileReader(dnd_javascript);
        BufferedReader bufferedReader = new BufferedReader(reader);
        StringBuilder builder = new StringBuilder();
        bufferedReader.lines().forEach(
                o->builder.append(o).append('\n')
        );

        String javaScript = builder.toString();
        javaScript = javaScript + " simulateDragDrop(document.getElementById('column-a'),document.getElementById('column-b'));";
        Selenide.executeJavaScript(javaScript);

        //Проверяет что элемент КолонкаА содержит текст Б
        //element(By.cssSelector("#column-a")).shouldHave(text("B"));
        //drugAndDropJavaScriptPage.shouldCheckColumnATextOnDrugAndDropJavaScriptPage("B");
        //assertEquals(drugAndDropJavaScriptPage.getColumnAOnDrugAndDropJavaScriptPage().getOwnText(), "B");
        drugAndDropJavaScriptPage.getColumnAOnDrugAndDropJavaScriptPage().shouldHave(text("B"));

    }

    public static void downloadFile() throws IOException {
        //Открыть браузер
        startBrowser();

        //Убедиться что на странице есть слова "Welcome to the-internet"
        assertEquals(homePage.getTextWelcomeOnHomePage().getOwnText(), "Welcome to the-internet");
        //Убедиться что ссылка содержит слова "File Download"
        assertEquals(homePage.getTextFileDownloadOnHomePage().getOwnText(), "File Download");
        //перейти по ссылке "File Download"
        homePage.getTextFileDownloadOnHomePage().click();

        //Производит загрузку файла some-file.txt с сервера в папку build/downloads
        //File report = $(By.xpath("//a[contains(text(),'some-file.txt')]")).download();
        File report = downloadFilePage.getDownloadLink().download();

        //Проверяет, что нужный файл скачан с сервера
        assertEquals(report.getName(), "some-file.txt");

        //Выводит в консоль весь путь до файла
        System.out.println(report + " Путь до файла");
        //Выводит в консоль только название файла
        System.out.println(report.getName()+" Имя файла");

        //Удаляет папку downloads с загруженным файлом some-file.txt
        FileUtils.deleteDirectory(new File("build/downloads"));
        
    }

    public static void closeBrowser(){
        Selenide.clearBrowserCookies();
        Selenide.closeWindow();
    }
}
